package sprise.movieudacity.handler;

import android.os.Message;

import sprise.movieudacity.model.ReviewResponse;
import sprise.movieudacity.view.TrailerReviewFragment;

/**
 * Created by SRIN on 7/13/2016.
 */
public class ReviewHandler extends BaseHandler<TrailerReviewFragment> {
    public static final int WHAT_DISPLAY_REVIEW = 1;
    public static final int WHAT_DIMISS_LOADING = 2;

    private TrailerReviewFragment mFragment;

    public ReviewHandler(TrailerReviewFragment parentComponent) {
        super(parentComponent);
        this.mFragment = parentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case WHAT_DISPLAY_REVIEW:
                showReviewList((ReviewResponse) msg.obj);
                break;
            case WHAT_DIMISS_LOADING:
                mFragment.dismissProgressBar();
        }
        super.handleMessage(msg);
    }

    public void showReviewList(ReviewResponse response){
        mFragment.showReview(response);
    }
}
