package sprise.movieudacity.handler;

import android.os.Message;

import sprise.movieudacity.model.TrailerResponse;
import sprise.movieudacity.view.ListTrailerActivity;

/**
 * Created by SRIN on 7/14/2016.
 */
public class TrailerHandler extends BaseHandler<ListTrailerActivity> {
    private static final String TAG = TrailerHandler.class.getSimpleName();

    public final static int WHAT_SHOW_TRAILER_LIST = 1;
    public final static int WHAT_DISMISS_PROGRESS_DIALOG = 2;

    private ListTrailerActivity mActivity;

    public TrailerHandler(ListTrailerActivity parentComponent) {
        super(parentComponent);
        this.mActivity = parentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case WHAT_SHOW_TRAILER_LIST:
                showTrailerList((TrailerResponse) msg.obj);
                break;
            case WHAT_DISMISS_PROGRESS_DIALOG:
                mActivity.dismissLoading();
                break;
        }
    }

    public void showTrailerList(TrailerResponse response){
        mActivity.showListTrailer(response);
    }
}
