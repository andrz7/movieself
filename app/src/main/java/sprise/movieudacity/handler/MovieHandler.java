package sprise.movieudacity.handler;

import android.os.Message;

import sprise.movieudacity.model.PopularResponse;
import sprise.movieudacity.view.MovieListFragment;

/**
 * Created by SRIN on 7/12/2016.
 */
public class MovieHandler extends BaseHandler<MovieListFragment>{
    public final static int WHAT_DISPLAY_LIST = 1;
    public final static int WHAT_DISMISS_LOADING_BAR = 2;
    public final static int WHAT_DISPLAY_LIST_TOP_RATED = 3;


    private MovieListFragment mFragment;

    public MovieHandler(MovieListFragment parentComponent) {
        super(parentComponent);
        this.mFragment = parentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case WHAT_DISPLAY_LIST:
                showListMovies((PopularResponse) msg.obj);
                break;
            case WHAT_DISMISS_LOADING_BAR:
                mFragment.dismissProgressBar();
                break;
            case WHAT_DISPLAY_LIST_TOP_RATED:
                break;
        }
        super.handleMessage(msg);
    }

    public void showListMovies(PopularResponse response){
        mFragment.showMovieList(response);
    }
}
