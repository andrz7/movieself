package sprise.movieudacity.handler;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;

import sprise.movieudacity.GlobalApplication;

/**
 * Created by SRIN on 7/12/2016.
 */
public class BaseHandler<T> extends Handler {

    private Context mContext;
    private T mParentComponent;
    private GlobalApplication mApplication;

    @SuppressLint("NewApi")
    public BaseHandler(T parentComponent){
        mParentComponent = parentComponent;
        if(mParentComponent instanceof Activity) {
            mApplication = (GlobalApplication) ((Activity) mParentComponent).getApplication();
        } else if(mParentComponent instanceof Fragment) {
            mApplication = (GlobalApplication) ((Fragment) mParentComponent).getActivity().getApplication();
        } else if(mParentComponent instanceof android.app.Fragment) {
            mApplication = (GlobalApplication) ((android.app.Fragment) mParentComponent).getActivity().getApplication();
        } else {
            throw new IllegalArgumentException("Parent component must be instance of activity, fragment, or support fragment");
        }
        mContext = mApplication.getApplicationContext();
    }

    @SuppressLint("NewApi")
    private Activity getParentActivity() {
        if(mParentComponent instanceof Activity) {
            return (Activity) mParentComponent;
        } else if(mParentComponent instanceof Fragment) {
            return ((Fragment) mParentComponent).getActivity();
        } else if(mParentComponent instanceof android.app.Fragment) {
            return ((android.app.Fragment) mParentComponent).getActivity();
        } else {
            return null;
        }
    }
}
