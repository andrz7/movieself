package sprise.movieudacity.services;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import sprise.movieudacity.util.Constant;

/**
 * Created by SRIN on 7/12/2016.
 */
public class Client {
    private static ClientServices restService;
    private static ClientServices mClient;
    private static ClientServices mAuthClient;
    private static ClientServices mDummyClient;
    private static ClientServices mAuthDummyClient;
    private static String versionCode;
    private static GsonBuilder sGsonBuilder;

    private Client() {}

    public static ClientServices getService(Context context){
        if (mClient == null){
            // Changes to support the usage of custom Gson deserializer (g.hamidy)
            RestAdapter.Builder builder = new RestAdapter.Builder();
            builder.setEndpoint(Constant.BASE_URL);
            builder.setLogLevel(RestAdapter.LogLevel.FULL);
            builder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Content-Type", "application/json");
                }
            }).setClient(new OkClient(getOkHttpClient()));;

            if(sGsonBuilder != null)
                builder.setConverter(new GsonConverter(sGsonBuilder.create()));

            RestAdapter adapter = builder.build();
            mClient = adapter.create(ClientServices.class);

            versionCode = getVersionCode(context);
        }
        return mClient;
    }

    public static OkHttpClient getOkHttpClient () {
        OkHttpClient client = getUnsafeOkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        return client;
    }

    public static OkHttpClient getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            } };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.setSslSocketFactory(sslSocketFactory);
            okHttpClient.setHostnameVerifier(new HostnameVerifier() {

                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return Constant.BASE_URL.contains(hostname);
                }
            });

            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private static String getVersionCode(Context context) {
        PackageInfo info = null;
        try {
            info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(info != null) {
            return String.format("%d", info.versionCode);
        }

        return "";
    }
}
