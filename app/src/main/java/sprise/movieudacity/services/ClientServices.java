package sprise.movieudacity.services;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import sprise.movieudacity.model.PopularResponse;
import sprise.movieudacity.model.ReviewResponse;
import sprise.movieudacity.model.TrailerResponse;

/**
 * Created by Andrea's on 7/11/2016.
 */
public interface ClientServices {
    @GET("/movie/popular/")
    void getMovieList(@Query("api_key") String apiKey, Callback<PopularResponse> callback);

    @GET("/movie/top_rated/")
    void getMovieListByRating(@Query("api_key") String apiKey, Callback<PopularResponse> callback);

    @GET("/movie/{id}/reviews")
    void getMovieReview(@Path("id")long id, @Query("api_key") String apiKey, Callback<ReviewResponse> callback);

    @GET("/movie/{id}/videos")
    void getMovieTrailer(@Path("id")long id, @Query("api_key") String apiKey, Callback<TrailerResponse> callback);
}
