package sprise.movieudacity.adapter;

import android.view.View;

/**
 * Created by SRIN on 7/12/2016.
 */
public interface CustomItemClickListener {
    void onItemClick(View v, int position);
}
