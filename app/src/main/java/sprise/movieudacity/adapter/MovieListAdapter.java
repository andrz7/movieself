package sprise.movieudacity.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import sprise.movieudacity.R;
import sprise.movieudacity.model.Movie;
import sprise.movieudacity.util.Constant;
import sprise.movieudacity.util.ImageSize;

/**
 * Created by Andrea's on 7/11/2016.
 */
public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.Holder>{
    private CustomItemClickListener listener;
    private ArrayList<Movie> data;
    private Context mContext;

    public MovieListAdapter(Context context, CustomItemClickListener listener) {
        mContext = context;
        this.listener = listener;
    }

    public MovieListAdapter(Context context) {
        mContext = context;
    }

    public void setData(ArrayList<Movie> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.movie_list_card_item, parent, false);
        final Holder holder = new Holder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Uri uri = Uri.parse(Constant.BASE_IMAGE_URL).buildUpon()
                .appendPath(ImageSize.w342.getValue())
                .appendPath(data.get(position).getPosterPath().replace("/", ""))
                .build();

        Picasso.with(mContext).load(uri.toString()).into(holder.movieImage);
        holder.releaseDateTv.setText(data.get(position).getReleaseDate());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        private TextView releaseDateTv;
        private ImageView movieImage;

        public Holder(View itemView) {
            super(itemView);
            movieImage = (ImageView) itemView.findViewById(R.id.poster);
            releaseDateTv = (TextView) itemView.findViewById(R.id.release_date);
        }
    }
}
