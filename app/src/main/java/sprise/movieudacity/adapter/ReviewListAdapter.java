package sprise.movieudacity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sprise.movieudacity.R;
import sprise.movieudacity.model.Review;

/**
 * Created by SRIN on 7/13/2016.
 */
public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.Holder>{
    private List<Review> data;
    private Context mContext;

    public ReviewListAdapter(Context context) {
        mContext = context;
    }

    public void setData(List<Review> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.review_card_item, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.authorTv.setText(data.get(position).getAuthor());
        holder.descTv.setText(data.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView authorTv, descTv;

        public Holder(View itemView) {
            super(itemView);
            authorTv = (TextView) itemView.findViewById(R.id.author_name);
            descTv = (TextView) itemView.findViewById(R.id.review_desc_tv);
        }
    }
}
