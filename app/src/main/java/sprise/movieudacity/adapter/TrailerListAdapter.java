package sprise.movieudacity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sprise.movieudacity.R;
import sprise.movieudacity.model.Trailer;

/**
 * Created by SRIN on 7/14/2016.
 */
public class TrailerListAdapter extends RecyclerView.Adapter<TrailerListAdapter.Holder>{
    private CustomItemClickListener listener;
    private List<Trailer> data;
    private Context mContext;

    public TrailerListAdapter(Context mContext, CustomItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
    }

    public  void setData(List<Trailer> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.trailer_list_card_item, parent, false);
        final Holder holder = new Holder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, holder.getAdapterPosition());
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.videoName.setText(data.get(position).getName());
        holder.videoType.setText(data.get(position).getType());
        holder.videoSite.setText(data.get(position).getSite());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView videoName, videoType, videoSite;

        public Holder(View itemView) {
            super(itemView);
            videoName = (TextView) itemView.findViewById(R.id.video_name);
            videoType = (TextView) itemView.findViewById(R.id.video_type);
            videoSite = (TextView) itemView.findViewById(R.id.site);
        }
    }
}
