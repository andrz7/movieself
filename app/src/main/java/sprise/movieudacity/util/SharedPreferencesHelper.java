package sprise.movieudacity.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sprise.movieudacity.model.Movie;

/**
 * Created by SRIN on 7/14/2016.
 */
public class SharedPreferencesHelper {
    public static final String FAVORITES = "Product_Favorite";
    public static final String PREFS_NAME = "PRODUCT_APP";
    public static final String PREFS_NAME_DETAIL = "DETAIL_APP";
    public static final String KEY_MOVIE_OBJ = "movie";
    public static String KEY_TWO_PANE = "two_pane";

    public SharedPreferencesHelper() {
        super();
    }

    static SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /*
        this method is used to know if the app use two pane or single pane
    * */

    public static void setTwoPane(Context context, boolean isTwoPane){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(KEY_TWO_PANE, isTwoPane);
        editor.commit();
    }

    public static boolean getTwoPane(Context context){
        return getSharedPreferences(context).getBoolean(KEY_TWO_PANE, false);
    }

    /*
        This four methods are used for maintaining favorites.
    */

    public void saveFavorites(Context context, List<Movie> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(FAVORITES, jsonFavorites);

        editor.commit();
    }

    public void addFavorite(Context context, Movie movie) {
        List<Movie> favorites = getFavorites(context);
        if (favorites == null)
            favorites = new ArrayList<Movie>();
        favorites.add(movie);
        saveFavorites(context, favorites);
    }

    public void removeFavorite(Context context, Movie movie) {
        List<Movie> favorites = getFavorites(context);
        if (favorites != null) {
            favorites.remove(indexOf(context, movie.getId()));
            saveFavorites(context, favorites);
        }
    }

    public ArrayList<Movie> getFavorites(Context context) {
        SharedPreferences settings;
        ArrayList<Movie> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Movie[] favoriteItems = gson.fromJson(jsonFavorites,
                    Movie[].class);

            favorites = new ArrayList<Movie>(Arrays.asList(favoriteItems));
        } else
            return null;

        return favorites;
    }

    public boolean isContain(Context context, long id){
        SharedPreferences settings;
        List<Movie> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Movie[] favoriteItems = gson.fromJson(jsonFavorites,
                    Movie[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Movie>(favorites);
            for (Movie movie : favorites){
                if (movie.getId() == id)
                    return true;
            }
        }
        return false;
    }

    public int indexOf(Context context, long id){
        SharedPreferences settings;
        List<Movie> favorites;

        settings = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);

        if (settings.contains(FAVORITES)) {
            String jsonFavorites = settings.getString(FAVORITES, null);
            Gson gson = new Gson();
            Movie[] favoriteItems = gson.fromJson(jsonFavorites,
                    Movie[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Movie>(favorites);
            for (int i = 0 ; i < favorites.size() ; i++){
                if (id == favorites.get(i).getId())
                    return i;
            }
        }
        return -1;
    }


    public void saveChosenDetail(Context context, Movie favorites) {
        SharedPreferences.Editor editor;
        SharedPreferences settings;

        settings = context.getSharedPreferences(PREFS_NAME_DETAIL,
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(KEY_TWO_PANE, jsonFavorites);

        editor.commit();
    }

    public void addChosenDetail(Context context, Movie movie) {
        saveChosenDetail(context, movie);
    }

    public Movie getChosenDetail(Context context) {
        SharedPreferences settings;
        Movie chosenMovie;

        settings = context.getSharedPreferences(PREFS_NAME_DETAIL,
                Context.MODE_PRIVATE);

        if (settings.contains(KEY_TWO_PANE)) {
            String jsonFavorites = settings.getString(KEY_TWO_PANE, null);
            Gson gson = new Gson();
            chosenMovie = gson.fromJson(jsonFavorites,
                    Movie.class);
        } else
            return null;

        return chosenMovie;
    }
}
