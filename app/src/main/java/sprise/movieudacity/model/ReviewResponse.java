package sprise.movieudacity.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SRIN on 7/13/2016.
 */
public class ReviewResponse {
    @SerializedName("id")
    long id;
    @SerializedName("page")
    int page;
    @SerializedName("results")
    private List<Review> data;

    public long getId() {
        return id;
    }

    public int getPage() {
        return page;
    }

    public List<Review> getData() {
        return data;
    }
}
