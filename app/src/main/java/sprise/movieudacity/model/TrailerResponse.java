package sprise.movieudacity.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SRIN on 7/14/2016.
 */
public class TrailerResponse {
    @SerializedName("id")
    private long id;
    @SerializedName("results")
    private List<Trailer> data;

    public List<Trailer> getData() {
        return data;
    }
}
