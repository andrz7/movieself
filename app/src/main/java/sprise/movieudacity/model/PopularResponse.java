package sprise.movieudacity.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrea's on 7/11/2016.
 */
public class PopularResponse {
    @SerializedName("page")
    private int page;
    @SerializedName("results")
    private ArrayList<Movie> data;

    public int getPage() {
        return page;
    }

    public ArrayList<Movie> getData() {
        return data;
    }
}
