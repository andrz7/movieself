package sprise.movieudacity.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Andrea's on 7/11/2016.
 */
public class Movie implements Parcelable, Comparable<Movie>{
    @SerializedName("poster_path")
    public final String posterPath;
    @SerializedName("overview")
    public final String overview;
    @SerializedName("release_date")
    public final String releaseDate;
    @SerializedName("id")
    public final long id;
    @SerializedName("original_title")
    public final String originalTitle;
    @SerializedName("title")
    public final String title;
    @SerializedName("popularity")
    public final double popularity;
    @SerializedName("vote_count")
    public final int voteCount;
    @SerializedName("vote_average")
    public final double voteAverage;

    public Movie(String posterPath, String overview, String releaseDate, long id,
                 String originalTitle, String title, double popularity, int voteCount,
                 int voteAverage) {
        this.posterPath = posterPath;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.id = id;
        this.originalTitle = originalTitle;
        this.title = title;
        this.popularity = popularity;
        this.voteCount = voteCount;
        this.voteAverage = voteAverage;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public String getOverview() {
        return overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public long getId() {
        return id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public String getTitle() {
        return title;
    }

    public double getPopularity() {
        return popularity;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    @Override
    public int compareTo(Movie another) {
        return id < another.getId() ? -1 : id > another.getId() ? 1 : 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(voteCount);
        dest.writeDouble(voteAverage);
        dest.writeDouble(popularity);
        dest.writeString(originalTitle);
        dest.writeString(releaseDate);
        dest.writeString(posterPath);
        dest.writeString(overview);
        dest.writeString(title);
    }

    public static final Parcelable.Creator<Movie> CREATOR =
            new Creator<Movie>() {
                @Override
                public Movie createFromParcel(Parcel source) {
                    return new Movie(source);
                }

                @Override
                public Movie[] newArray(int size) {
                    return new Movie[size];
                }
            };

    private Movie(Parcel in){
        this.id = in.readLong();
        this.voteCount = in.readInt();
        this.voteAverage = in.readDouble();
        this.popularity = in.readDouble();
        this.originalTitle = in.readString();
        this.releaseDate = in.readString();
        this.posterPath = in.readString();
        this.overview = in.readString();
        this.title = in.readString();
    }
}
