package sprise.movieudacity.controller;

import android.content.Context;
import android.os.Message;
import android.view.View;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sprise.movieudacity.GlobalApplication;
import sprise.movieudacity.handler.MovieHandler;
import sprise.movieudacity.model.Movie;
import sprise.movieudacity.model.PopularResponse;
import sprise.movieudacity.services.Client;
import sprise.movieudacity.services.ClientServices;
import sprise.movieudacity.util.Constant;
import sprise.movieudacity.view.MovieListFragment;

/**
 * Created by Andrea's on 7/11/2016.
 */
public class MovieListController {

    private GlobalApplication globalApplication;
    private MovieListFragment mFragment;
    private MovieHandler mHandler;
    private Context mContext;
    private List<Movie> data;

    public MovieListController(MovieListFragment mFragment) {
        this.mFragment = mFragment;
        this.mContext = mFragment.getAttachedActivity().getApplicationContext();
        this.mHandler = new MovieHandler(mFragment);
        this.globalApplication = (GlobalApplication) mFragment.getAttachedActivity().getApplication();
    }

    public void getMovieList() {
        mFragment.loading.setVisibility(View.VISIBLE);
        ClientServices services = Client.getService(mContext);
        services.getMovieList(Constant.API_KEY, new Callback<PopularResponse>() {
            @Override
            public void success(PopularResponse popularResponse, Response response) {
                mHandler.sendEmptyMessage(MovieHandler.WHAT_DISMISS_LOADING_BAR);
                loadMovieList(popularResponse);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void getMovieListByRating(){
        mFragment.loading.setVisibility(View.VISIBLE);
        ClientServices services = Client.getService(mContext);
        services.getMovieListByRating(Constant.API_KEY, new Callback<PopularResponse>() {
            @Override
            public void success(PopularResponse response, Response response2) {
                mHandler.sendEmptyMessage(MovieHandler.WHAT_DISMISS_LOADING_BAR);
                loadMovieList(response);
            }
            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void loadMovieList(PopularResponse response){
        if (mFragment != null){
            Message message = new Message();
            message.what = MovieHandler.WHAT_DISPLAY_LIST;
            message.obj = response;
            mHandler.sendMessage(message);
        }
    }

    private void loadMovieListByRate(PopularResponse response){
        if (mFragment != null){
            Message message = new Message();
            message.what = MovieHandler.WHAT_DISPLAY_LIST;
            message.obj = response;
            mHandler.sendMessage(message);
        }
    }
}