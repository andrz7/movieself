package sprise.movieudacity.controller;

import android.content.Context;
import android.os.Message;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sprise.movieudacity.GlobalApplication;
import sprise.movieudacity.handler.ReviewHandler;
import sprise.movieudacity.model.Review;
import sprise.movieudacity.model.ReviewResponse;
import sprise.movieudacity.services.Client;
import sprise.movieudacity.services.ClientServices;
import sprise.movieudacity.util.Constant;
import sprise.movieudacity.view.TrailerReviewFragment;

/**
 * Created by SRIN on 7/13/2016.
 */
public class MovieReviewController {
    private final String TAG = MovieReviewController.class.getSimpleName();

    private GlobalApplication globalApplication;
    private TrailerReviewFragment mFragment;
    private ReviewHandler mHandler;
    private Context mContext;
    private List<Review> data;

    public MovieReviewController(TrailerReviewFragment mFragment) {
        this.mFragment = mFragment;
        this.mContext = mFragment.getAttachedActivity().getApplicationContext();
        this.mHandler = new ReviewHandler(mFragment);
        this.globalApplication = (GlobalApplication) mFragment.getAttachedActivity().getApplication();
    }

    public void getMovieReview(long id){
        ClientServices services = Client.getService(mContext);
        services.getMovieReview(id, Constant.API_KEY, new Callback<ReviewResponse>() {
            @Override
            public void success(ReviewResponse reviewResponse, Response response) {
                mHandler.sendEmptyMessage(ReviewHandler.WHAT_DIMISS_LOADING);
                loadReviewList(reviewResponse);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void loadReviewList(ReviewResponse response){
        if (mFragment != null){
            Message message = new Message();
            message.what = ReviewHandler.WHAT_DISPLAY_REVIEW;
            message.obj = response;
            mHandler.sendMessage(message);
        }
    }
}
