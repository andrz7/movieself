package sprise.movieudacity.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sprise.movieudacity.GlobalApplication;
import sprise.movieudacity.handler.TrailerHandler;
import sprise.movieudacity.model.Trailer;
import sprise.movieudacity.model.TrailerResponse;
import sprise.movieudacity.services.Client;
import sprise.movieudacity.services.ClientServices;
import sprise.movieudacity.util.Constant;
import sprise.movieudacity.view.ListTrailerActivity;

/**
 * Created by SRIN on 7/14/2016.
 */
public class TrailerListController {
    private GlobalApplication application;
    private ListTrailerActivity mActivity;
    private List<Trailer> data;
    private Handler mHandler;
    private Context mContext;

    public TrailerListController(ListTrailerActivity mActivity) {
        this.mActivity = mActivity;
        this.mContext = mActivity.getApplicationContext();
        this.mHandler = new TrailerHandler(mActivity);
        this.application = (GlobalApplication) mActivity.getApplication();
    }

    public void getMovieTrailer(long id){
        ClientServices services = Client.getService(mContext);
        services.getMovieTrailer(id, Constant.API_KEY, new Callback<TrailerResponse>() {
            @Override
            public void success(TrailerResponse trailerResponse, Response response) {
                mHandler.sendEmptyMessage(TrailerHandler.WHAT_DISMISS_PROGRESS_DIALOG);
                loadMovieTrailer(trailerResponse);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void loadMovieTrailer(TrailerResponse response){
        if (response != null){
            Message message = new Message();
            message.what = TrailerHandler.WHAT_SHOW_TRAILER_LIST;
            message.obj = response;
            mHandler.sendMessage(message);
        }
    }

}
