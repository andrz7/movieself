package sprise.movieudacity.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import sprise.movieudacity.R;
import sprise.movieudacity.adapter.ReviewListAdapter;
import sprise.movieudacity.controller.MovieReviewController;
import sprise.movieudacity.model.Movie;
import sprise.movieudacity.model.Review;
import sprise.movieudacity.model.ReviewResponse;

/**
 * Created by SRIN on 7/13/2016.
 */
public class TrailerReviewFragment extends Fragment {
    private final String TAG = TrailerReviewFragment.class.getSimpleName();

    private ProgressBar loadingReview;
    private RecyclerView reviewRv;

    private LinearLayoutManager layoutManager;
    private MovieReviewController controller;
    private ReviewListAdapter adapter;
    private Activity mActivity;
    private List<Review> data;
    private Movie movie;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trailer_review_fragment, container, false);
        loadingReview = (ProgressBar) view.findViewById(R.id.loading_review);
        reviewRv = (RecyclerView) view.findViewById(R.id.review_rv);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMovieObj();
        controller = new MovieReviewController(this);
        data = new ArrayList<>();

        layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        reviewRv.setLayoutManager(layoutManager);

        controller.getMovieReview(movie.getId());
    }

    public void showReview(ReviewResponse response){
        if (response != null){
            adapter = new ReviewListAdapter(mActivity);
            data = response.getData();
            adapter.setData(data);
            reviewRv.setAdapter(adapter);
        }
    }

    public void dismissProgressBar(){
        loadingReview.setVisibility(View.GONE);
    }

    private void getMovieObj(){
        DetailMovieActivity movieActivity = (DetailMovieActivity) getActivity();
        movie = movieActivity.getMovie();
    }

    public static TrailerReviewFragment newInstance(){
        TrailerReviewFragment fragment = new TrailerReviewFragment();
        return fragment;
    }

    public Activity getAttachedActivity(){
        return mActivity;
    }
}
