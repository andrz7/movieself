package sprise.movieudacity.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import sprise.movieudacity.R;
import sprise.movieudacity.adapter.ViewPagerAdapter;
import sprise.movieudacity.model.Movie;

public class DetailMovieActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private TabLayout tablayout;
    private Toolbar toolbar;
    private Movie movie;

    public static final String KEY_MOVIE_ID = "movie_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);
        getMovieObj();

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setUpToolbar();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tablayout = (TabLayout) findViewById(R.id.tabs);
        tablayout.setupWithViewPager(viewPager);

    }

    private void getMovieObj(){
        movie = (Movie) getIntent().getParcelableExtra("movie");
    }

    public Movie getMovie(){
        return movie;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie_detail_trailer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.watch_trailer:
                Intent i = new Intent(this, ListTrailerActivity.class);
                i.putExtra(KEY_MOVIE_ID, movie.getId());
                startActivity(i);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) {
            throw new IllegalStateException("Activity must implement toolbar");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new DetailFragment(), "Detail");
        adapter.addFragment(new TrailerReviewFragment(), "Review");
        viewPager.setAdapter(adapter);
    }
}
