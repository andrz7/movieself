package sprise.movieudacity.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import sprise.movieudacity.R;
import sprise.movieudacity.adapter.MovieListAdapter;
import sprise.movieudacity.model.Movie;
import sprise.movieudacity.util.SharedPreferencesHelper;

public class FavoriteListActivity extends AppCompatActivity {
    private final static String TAG = FavoriteListActivity.class.getSimpleName();

    private final int SPAN_COUNT = 2;

    private MovieListAdapter adapter;
    private RecyclerView favoriteRv;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        favoriteRv = (RecyclerView) findViewById(R.id.favorite_rv);
        setupToolbar();
        setUpRecyclerView();

    }

    private void setupToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) {
            throw new IllegalStateException("Activity must implement toolbar");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpRecyclerView(){
        GridLayoutManager layoutManager = new GridLayoutManager(
                this,
                SPAN_COUNT,
                LinearLayoutManager.VERTICAL
                , false);
        favoriteRv.setLayoutManager(layoutManager);
        adapter = new MovieListAdapter(this);
        SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper();
        ArrayList<Movie> data = sharedPreferencesHelper.getFavorites(this);
        if (data.size() > 0){
            Collections.sort(data);
            adapter.setData(data);
            favoriteRv.setAdapter(adapter);
        }
    }
}
