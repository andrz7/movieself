package sprise.movieudacity.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import sprise.movieudacity.R;
import sprise.movieudacity.model.Movie;
import sprise.movieudacity.util.SharedPreferencesHelper;

public class MainActivity extends AppCompatActivity implements MovieListFragment.MovieSelected{
    private boolean mTwopane;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setUpToolbar();

        if (findViewById(R.id.movie_detail) != null){
            mTwopane = true;
            SharedPreferencesHelper.setTwoPane(this, true);
        }
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content, MovieListFragment.newInstance()).commit();
        }
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);

        if (getSupportActionBar() == null) {
            throw new IllegalStateException("Activity must implement toolbar");
        }
    }

    @Override
    public void onMovieSelected(Movie movie) {
        SharedPreferencesHelper helper = new SharedPreferencesHelper();
        helper.addChosenDetail(MainActivity.this, movie);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.movie_detail, DetailFragment.newInstance())
                .commit();
    }
}
