package sprise.movieudacity.view;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.like.LikeButton;
import com.like.OnLikeListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import sprise.movieudacity.R;
import sprise.movieudacity.model.Movie;
import sprise.movieudacity.util.Constant;
import sprise.movieudacity.util.ImageSize;
import sprise.movieudacity.util.SharedPreferencesHelper;

/**
 * Created by SRIN on 7/13/2016.
 */
public class DetailFragment extends Fragment implements OnLikeListener{
    private static final String TAG = DetailFragment.class.getSimpleName();

    private TextView movieTitleTv, movieRatingTv, movieSynopsisTv, timeTv;
    private ProgressBar progressBar;
    private ImageView posterImage;
    private Movie movie;

    private SharedPreferencesHelper sharedPreferencesHelper;
    private LikeButton likeButton;
    private Activity mActivity;

    public DetailFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_movie, container, false);
        movieSynopsisTv = (TextView) view.findViewById(R.id.description_tv);
        likeButton = (LikeButton) view.findViewById(R.id.favorite_button);
        movieTitleTv = (TextView) view.findViewById(R.id.movie_title_tv);
        posterImage = (ImageView) view.findViewById(R.id.poster_image);
        movieRatingTv = (TextView) view.findViewById(R.id.rating_tv);
        progressBar = (ProgressBar) view.findViewById(R.id.loading);
        timeTv = (TextView) view.findViewById(R.id.time_tv);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        likeButton.setOnLikeListener(this);

        sharedPreferencesHelper = new SharedPreferencesHelper();

        if (SharedPreferencesHelper.getTwoPane(mActivity)){
            movie = sharedPreferencesHelper.getChosenDetail(mActivity);
        }else{
            getMovieObj();
        }

        setAllComponent();

        if (sharedPreferencesHelper.isContain(mActivity, movie.getId()))
            likeButton.setLiked(true);
    }

    private void getMovieObj(){
        DetailMovieActivity movieActivity = (DetailMovieActivity) getActivity();
        movie = movieActivity.getMovie();
    }

    public static DetailFragment newInstance(){
        DetailFragment fragment = new DetailFragment();
        return fragment;
    }

    private void setAllComponent(){
        movieTitleTv.setText(movie.getTitle());
        movieSynopsisTv.setText(movie.getOverview());
        movieRatingTv.setText(String.valueOf(movie.getVoteAverage()) + " / 10");
        timeTv.setText("Release date : " + movie.getReleaseDate());

        Uri uri = Uri.parse(Constant.BASE_IMAGE_URL).buildUpon()
                .appendPath(ImageSize.w1000.getValue())
                .appendPath(movie.getPosterPath().replace("/", ""))
                .build();

        Picasso.with(mActivity).load(uri.toString()).into(posterImage, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
    }

    @Override
    public void liked(LikeButton likeButton) {
        sharedPreferencesHelper.addFavorite(mActivity, movie);
    }

    @Override
    public void unLiked(LikeButton likeButton) {
        sharedPreferencesHelper.removeFavorite(mActivity, movie);
    }
}
