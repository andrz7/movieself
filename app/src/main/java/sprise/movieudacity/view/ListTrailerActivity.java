package sprise.movieudacity.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import sprise.movieudacity.R;
import sprise.movieudacity.adapter.CustomItemClickListener;
import sprise.movieudacity.adapter.TrailerListAdapter;
import sprise.movieudacity.controller.TrailerListController;
import sprise.movieudacity.model.Trailer;
import sprise.movieudacity.model.TrailerResponse;
import sprise.movieudacity.util.Constant;

public class ListTrailerActivity extends AppCompatActivity {
    private static final String TAG = ListTrailerActivity.class.getSimpleName();

    private RecyclerView trailerList;
    private ProgressBar loading;
    private Toolbar toolbar;

    private TrailerListController mController;
    private TrailerListAdapter adapter;

    private List<Trailer> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_trailer);

        trailerList = (RecyclerView) findViewById(R.id.trailer_list_review);
        loading = (ProgressBar) findViewById(R.id.loading_trailer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        mController = new TrailerListController(this);

        setupToolbar();
        setRecyclerView();

        mController.getMovieTrailer(getChosenMovieId());
    }

    private void setRecyclerView(){
        adapter = new TrailerListAdapter(this, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                String link = Constant.YOUTUBE_URL_PATH + data.get(position).getKey();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this
                , LinearLayoutManager.VERTICAL, false);

        trailerList.setLayoutManager(layoutManager);
    }

    private void setupToolbar(){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) {
            throw new IllegalStateException("Activity must implement toolbar");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public long getChosenMovieId(){
        return getIntent().getExtras().getLong(DetailMovieActivity.KEY_MOVIE_ID);
    }

    public void showListTrailer(TrailerResponse response){
        if (response != null){
            data = response.getData();
            adapter.setData(data);
            trailerList.setAdapter(adapter);
        }
    }

    public void dismissLoading(){
        loading.setVisibility(View.GONE);
    }
}
