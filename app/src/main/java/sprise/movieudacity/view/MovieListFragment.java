package sprise.movieudacity.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import sprise.movieudacity.R;
import sprise.movieudacity.adapter.CustomItemClickListener;
import sprise.movieudacity.adapter.MovieListAdapter;
import sprise.movieudacity.controller.MovieListController;
import sprise.movieudacity.model.Movie;
import sprise.movieudacity.model.PopularResponse;
import sprise.movieudacity.util.Constant;
import sprise.movieudacity.util.SharedPreferencesHelper;

/**
 * Created by Andrea's on 7/11/2016.
 */
public class MovieListFragment extends Fragment {
    public final String TAG = MovieListFragment.class.getSimpleName();

    private RecyclerView listMovieRecycler;
    public ProgressBar loading;

    private GridLayoutManager layoutManager;
    private MovieListController controller;
    private MovieListAdapter adapter;
    private Activity mActivity;
    private ArrayList<Movie> data;

    public static final int SPAN_COUNT = 2;
    private boolean isDataExist = false;

    public interface MovieSelected{
        void onMovieSelected(Movie movie);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Constant.MOVIE_KEY, data);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState == null || !savedInstanceState.containsKey(Constant.MOVIE_KEY)){
            data = new ArrayList<>();
            isDataExist = false;
        }else {
            data = savedInstanceState.getParcelableArrayList(Constant.MOVIE_KEY);
            isDataExist = true;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);

        listMovieRecycler = (RecyclerView) view.findViewById(R.id.recyclerview);
        loading = (ProgressBar) view.findViewById(R.id.loading);

        controller = new MovieListController(this);

        layoutManager = new GridLayoutManager(
                mActivity,
                SPAN_COUNT,
                LinearLayoutManager.VERTICAL,
                false);

        listMovieRecycler.setLayoutManager(layoutManager);

        adapter = new MovieListAdapter(mActivity, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                if (SharedPreferencesHelper.getTwoPane(mActivity)){
                    ((MovieSelected) getActivity()).onMovieSelected(data.get(position));
                }else{
                    Intent i = new Intent(mActivity, DetailMovieActivity.class);
                    i.putExtra("movie", (Parcelable) data.get(position));
                    startActivity(i);
                }
            }
        });
        adapter.setData(data);
        listMovieRecycler.setAdapter(adapter);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isDataExist == false) {
            controller.getMovieList();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.movie_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.list_by_popularity:
                controller.getMovieList();
                break;
            case R.id.list_by_top_rated:
                controller.getMovieListByRating();
                break;
            case R.id.favorite:
                startActivity(new Intent(mActivity, FavoriteListActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    public void showMovieList(PopularResponse response){
        if (response != null){
            data = response.getData();
            adapter.setData(data);
        }
    }

    public void dismissProgressBar(){
        loading.setVisibility(View.GONE);
    }

    public static MovieListFragment newInstance(){
        MovieListFragment fragment = new MovieListFragment();
        return fragment;
    }

    public Activity getAttachedActivity(){
        return mActivity;
    }
}
