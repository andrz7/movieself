package sprise.movieudacity;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Handler;

import java.util.HashMap;
import java.util.List;

import sprise.movieudacity.model.Movie;
import sprise.movieudacity.services.ClientServices;

/**
 * Created by SRIN on 7/12/2016.
 */
public class GlobalApplication extends Application{
    private Class<? extends Activity> mTopActivityClass;
    private ClientServices mClient;
    private Context mAppContext;
    private Handler mHandler;

    public boolean isLoading;

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
        mAppContext = getApplicationContext();
        isLoading = false;
    }
}
